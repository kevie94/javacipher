/**
 * Created by Caoimhín on 20/03/2017.
 */
public class VigenereCipher {
    private String alphabet = "!#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[] ^_`abcdefghijklmnopqrstuvwxyz{|}~";

    public String encrypt(String message, String key){
        return translateMessage(message, key, true);
    }

    public String decrypt(String message, String key){
        return translateMessage(message, key, false);
    }

    public String translateMessage(String message, String key, boolean encode){
        String translated = "";
        int keyIndex = 0;
        int tNum = 0; //translated number
        for(char i: message.toCharArray()){
            int mNum = alphabet.indexOf(i); //message index number in alphabet
            int kNum = alphabet.indexOf(key.charAt(keyIndex)); //index of key character in alphabet

            if(encode == true){
                tNum = mNum + kNum;
            }else{
                tNum = mNum - kNum;
            }
            if(tNum < 0){
                tNum = tNum + alphabet.length();
            }
            else if(tNum >= alphabet.length()) {
                tNum = tNum - alphabet.length();
            }
            translated = translated.concat(String.valueOf(alphabet.charAt(tNum)));

            if(keyIndex == key.length()){
                keyIndex = 0;
            }

        }
        return translated;
    }
}
