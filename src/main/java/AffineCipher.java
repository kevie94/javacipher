import java.math.BigInteger;
import java.util.Random;

/**
 * Created by Caoimhín on 01/02/2017.
 */
public class AffineCipher {
    private String alphabet = "!#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[] ^_`abcdefghijklmnopqrstuvwxyz{|}~";
    Random generator = new Random(System.currentTimeMillis());
    CaesarCipher caesar = new CaesarCipher(alphabet);
    MultiplicativeCipher multiplicative = new MultiplicativeCipher(alphabet);

    public String encode(String message, int key){
        int[] keyParts = getKeyParts(key);
        message = multiplicative.encode(message, keyParts[1]);
        message = caesar.CaesarCipher(message, true, keyParts[0]);
        return message;
    }

    public String decode(String message, int key){
        int[] keyParts = getKeyParts(key);
        message = caesar.CaesarCipher(message, false, keyParts[0]);
        message = multiplicative.decode(message, keyParts[1]);
        return message;
    }

    public int[] getKeyParts(int key){
        int mKey = key / alphabet.length();
        int cKey = key % alphabet.length();
        int[] keyParts = {cKey, mKey};

        return keyParts;
    }

    public boolean checkKey(int key){
        int[] keyParts = getKeyParts(key);
        BigInteger mCheck = new BigInteger(Integer.toString(keyParts[1]));
        if(mCheck.gcd(new BigInteger(Integer.toString(alphabet.length()))).compareTo(new BigInteger("1")) == 0){
            return true;
        }else{
            return false;
        }
    }

    public int generateRandomKey(){
        BigInteger mCheck;
        int cKey = 0;
        int mKey = 0;
        BigInteger alphabetLength = new BigInteger(Integer.toString(alphabet.length()));
        BigInteger one = new BigInteger("1");
        while(true) {
            cKey = generator.nextInt(alphabet.length());
            mKey = generator.nextInt(alphabet.length());
            mCheck = new BigInteger(Integer.toString(mKey));
            if(mCheck.gcd(alphabetLength).compareTo(one) == 0){
                return (mKey * alphabet.length()) + cKey;
            }
        }
    }
}
