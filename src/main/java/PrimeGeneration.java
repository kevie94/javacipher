import java.math.BigInteger;
import java.util.Random;

/**
 * Created by Caoimhín on 12/02/2017.
 */
// Ended up not using any of this stuff in the later parts, but the rabin miller algorithm is certainly interesting.
public class PrimeGeneration {
    // Rabin miller checks if a number is prime, with a certainty of (1 - (1/2)^timesRun)
    // BigInteger has a constructor that performs this automatically, so I ended up using that.
    public boolean rabinMiller(BigInteger num){
        BigInteger d = num.subtract(new BigInteger("1"));
        int r = 0;
        while(d.mod(new BigInteger("2")).compareTo(new BigInteger("0")) == 0){
            d = d.divide(new BigInteger("2"));
            r++;
        }
        int trials = 5; //try to falsify prime 5 times.
        Random rand = new Random();
        trialLoop:
        for(int k = 0; k < trials; k++){
            BigInteger a;
            do {
                a = new BigInteger(1024, rand);
            }while(a.compareTo(new BigInteger("2"))== 1 && a.compareTo(num.subtract(new BigInteger("2")))== -1);
            BigInteger x = a.modPow(d, num);
            if(x.compareTo(new BigInteger("1")) == 0 || x.compareTo(num.subtract(new BigInteger("1"))) == 0) {
                continue trialLoop;
            }
            int i = 0;
            while(i < r){
                x = x.modPow(new BigInteger("2"), num);
                if(x.compareTo(new BigInteger("1")) == 0){
                    return false;
                }
                if(x.compareTo(num.subtract(new BigInteger("1"))) == 0){
                    continue trialLoop;
                }
                i++;
            }
            return false;
        }
        return true;
    }

}

