import java.util.Arrays;

public class TranspositionCipher {
    public String encrypt(String message, int key){
        String translated = "";
        if(key > message.length()){
            return message;
        }
        char[] msg = message.toCharArray();
        int count = 0;
        int rows = message.length() / key;
        if(message.length() % key > 0){
            rows++;
        }
        char transpArray[][] = new char[rows][key];

        for(int i = 0; i < rows; i++){
            for(int j = 0; j < key; j++){
                if(count >= message.length() ){
                    transpArray[i][j] = (char)0;  //if the character is trailing the end of the message
                    // I just add a null character (0 in ascii) to signify that it is a null character.
                }else{
                transpArray[i][j] = msg[count];
                count++;
                }
            }
        }
        for(int i = 0; i < key; i++){
            for(int j = 0; j < rows; j++){
                if(transpArray[j][i] != 0) {
                    translated = translated.concat(String.valueOf(transpArray[j][i]));
                }
            }
        }
        return translated;
    }

    public String decrypt(String message, int key){
        String translated = "";
        if(key > message.length()){
            return message;
        }
        char[] msg = message.toCharArray();
        int count = 0;
        int rows = message.length() / key ;
        if(message.length() % key > 0){
            rows++;
        }
        char transpArray[][] = new char[rows][key];
        int emptyCells = (key * rows) - message.length();
        for(int i = 0; i < key; i++){
            for(int j = 0; j < rows; j++){
                if(j == rows - 1 && i >= key - emptyCells || count >= message.length()){
                    //do nothing, as the space should be blank, or the end of the character sequence is finished.
                }
                else{
                transpArray[j][i] = msg[count];
                count++;
                }
            }
        }

        for(int i = 0; i < rows; i++){
            for(int j = 0; j < key; j++){
                if(transpArray[i][j] != 0) {
                    translated = translated.concat(String.valueOf(transpArray[i][j]));
                }
            }
        }
        return translated;
    }
}
