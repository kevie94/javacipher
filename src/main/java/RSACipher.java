import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Caoimhín on 08/03/2017.
 */
public class RSACipher {
    public static int DEFAULT_BLOCK_SIZE = 128;
    public static int BYTE_SIZE = 256;
    public static String CHARSET = "US-ASCII";

    public static void RSA(String filename,String key, char mode) throws FileNotFoundException, IOException{
        RSACipher cipher = new RSACipher();
        //encrypt
        if(mode == 'e'){
            cipher.encryptAndWriteToFile(filename, filename + "_Encrypted", key);
        }
        else if(mode == 'd'){
            cipher.readFromFileAndDecrypt(filename, filename + "_decrypted", key);
        }
        else{
            System.out.println("Error: " + mode + " is not a valid mode; choose 'e' for encrypt, 'd' for decrypt");
        }
    }

    public List<BigInteger> getBlocksFromText(String message){
        BigInteger BbyteSize = BigInteger.valueOf(Integer.valueOf(RSACipher.BYTE_SIZE));
        byte[] messageBytes = message.getBytes(Charset.forName(RSACipher.CHARSET));
        List<BigInteger> blockInts = new ArrayList<>();
        for(int blockStart = 0; blockStart <= messageBytes.length; blockStart += RSACipher.DEFAULT_BLOCK_SIZE){
            BigInteger blockInt = new BigInteger("0");
            for(int i = blockStart; i < (blockStart + RSACipher.DEFAULT_BLOCK_SIZE) && i < messageBytes.length; i++){
                BigInteger BmessageBytes = new BigDecimal((long)messageBytes[i]).toBigInteger();
                BigInteger x = BmessageBytes.multiply(BbyteSize.pow(i % RSACipher.DEFAULT_BLOCK_SIZE));

                blockInt = blockInt.add(x); //biginteger is immutable, value cannot change. Must assign
            }
            blockInts.add(blockInt);
        }
        return blockInts;
    }

    public String getTextFromBlocks(List<BigInteger> decryptedBlocks, int messageLength){
        String message = "";
        for(BigInteger block: decryptedBlocks){
            String blockMessage = "";
            for(int i = RSACipher.DEFAULT_BLOCK_SIZE - 1; i >= 0; i -= 1){
                if(message.length() + i < messageLength){
                    BigInteger asciiNum = block.divide(new BigDecimal(Math.pow(RSACipher.BYTE_SIZE, i)).toBigInteger());
                    block = block.mod(new BigDecimal(Math.pow(RSACipher.BYTE_SIZE, i)).toBigInteger());
                    blockMessage = blockMessage.concat(Character.toString((char)(asciiNum.intValue())));
                }
            }
            blockMessage = new StringBuilder(blockMessage).reverse().toString();
            message = message.concat(blockMessage);
        }
        System.out.println("Decrypted: " + message);
        return message;
    }

    public List<BigInteger> encryptMessage(String message, BigInteger n, BigInteger e){
        List<BigInteger> blockInts = getBlocksFromText(message);
        List<BigInteger> encryptedBlocks = new ArrayList<>();
        for(BigInteger block: blockInts){
            encryptedBlocks.add(block.modPow(e, n));
        }

        return encryptedBlocks;
    }

    public String decryptMessage(List<BigInteger> encryptedBlocks, BigInteger n, BigInteger d, int messageLength) {
        List<BigInteger> decryptedBlocks = new ArrayList<>();
        for(BigInteger block: encryptedBlocks){
            decryptedBlocks.add(block.modPow(d, n));
            System.out.println("block: " + block.modPow(d, n).toString());
        }
        return getTextFromBlocks(decryptedBlocks, messageLength);
    }

    public String encryptAndWriteToFile(String messageFilename, String encrytedFilename, String keyName){
        String[] key = readKeyFile(keyName);
        if(key == null){
            System.out.println("Error reading key from file.");
        }
        int keySize = Integer.parseInt(key[0]);
        BigInteger n = new BigInteger(key[1]);
        BigInteger e = new BigInteger(key[2]);
        String message = readFromFile(messageFilename);

        if(keySize < RSACipher.DEFAULT_BLOCK_SIZE * 8){
            //block size cannot be greater than keysize.
            System.out.println("Block Size is greater than Key Size. Cannot encrypt.");
            return null;
        }

        List<BigInteger> encryptedBlocks;
        encryptedBlocks = encryptMessage(message, n, e);
        String encryptedMessage = "";
        for(BigInteger i: encryptedBlocks){
            encryptedMessage = encryptedMessage.concat("," + i.toString());
        }
        encryptedMessage = (message.length() + "_" + RSACipher.DEFAULT_BLOCK_SIZE + "_" + encryptedMessage);
        writeToFile(encryptedMessage, encrytedFilename);
        return encryptedMessage;
    }

    public String readFromFileAndDecrypt(String messageFilename, String decrytedFilename, String keyName) {
        String[] key = readKeyFile(keyName);
        if(key == null){
            System.out.println("Error reading key from file.");
        }

        int keySize = Integer.parseInt(key[0]);
        BigInteger n = new BigInteger(key[1]);
        BigInteger d = new BigInteger(key[2]);
        String encryptedMessage = readFromFile(messageFilename);
        String[] splitRawMessage;
        String[] splitEncryptedMessage;
        List<BigInteger> encryptedBlocks = new ArrayList<>();

        splitRawMessage = encryptedMessage.split(",");
        splitEncryptedMessage = splitRawMessage[0].split("_");
        int messageLength = Integer.parseInt(splitEncryptedMessage[0]);
        int blockSize = Integer.parseInt(splitEncryptedMessage[1]);


        if(keySize < RSACipher.DEFAULT_BLOCK_SIZE * 8){
            //block size cannot be greater than keysize.
            System.out.println("Block Size is greater than Key Size. Cannot encrypt.");
            return null;
        }
        for(int i = 1; i < splitRawMessage.length; i++){
            encryptedBlocks.add(new BigInteger(splitRawMessage[i]));
        }
        String decryptedMessage = decryptMessage(encryptedBlocks, n, d, messageLength);
        writeToFile(decryptedMessage, decrytedFilename);
        return decryptedMessage;
    }

    public String[] readKeyFile(String keyFileName){
        String keyFile;
        try {
            keyFile = new String(Files.readAllBytes(Paths.get(keyFileName)));
            return keyFile.split(",");
        }catch(IOException e){
            e.printStackTrace();
        }
        System.out.println("Nothing read from file");
        return null;
    }

    public String readFromFile(String fileName){
        String message;
        try {
            message = new String(Files.readAllBytes(Paths.get(fileName)));
            return message;
        }catch(IOException e){
            e.printStackTrace();
            return null;
        }

    }

    public void writeToFile(String message, String filename){
        try{
            FileWriter messageWriter = new FileWriter(filename);
            messageWriter.write(message);
            messageWriter.close();
        }catch(IOException e){
            e.printStackTrace();
        }
    }
}
