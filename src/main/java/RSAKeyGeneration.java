import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Random;

// 'Textbook RSA' implementation
public class RSAKeyGeneration {
    public void makeKeyFiles(String name, int keysize) throws IOException{
        FileWriter pubKeyWriter = null;
        FileWriter privKeyWriter = null;
        try {
            pubKeyWriter = new FileWriter(name + "_publicKey.rsa");
            privKeyWriter = new FileWriter(name + "_privateKey.rsa");

            BigInteger[][] keys = generateKey(keysize);
            String publicString = keysize + "," + keys[0][0] + "," + keys[0][1];

            String privateString = keysize + "," + keys[1][0] + "," + keys[1][1];
            pubKeyWriter.write(publicString);
            privKeyWriter.write(privateString);
        }finally {
            if(pubKeyWriter != null) {
                pubKeyWriter.close();
            }
            if(privKeyWriter != null) {
                privKeyWriter.close();
            }
        }
    }

    public BigInteger[][] generateKey(int keysize){
        Random rand = new Random();
        BigInteger p = new BigInteger(keysize, 200, rand);
        BigInteger q = new BigInteger(keysize, 200, rand);
        BigInteger n = p.multiply(q);
        BigInteger e;
        BigInteger x;
        BigInteger pm = p.subtract(new BigInteger("1"));
        BigInteger qm = q.subtract(new BigInteger("1"));
        x = pm.multiply(qm);

        do{
            //e = new BigInteger;
            e = new BigInteger(keysize-1, rand);

        }while(e.gcd(x).compareTo(new BigInteger("1")) == 1 && e.compareTo(x) == -1);
        BigInteger d = e.modInverse(x);
        BigInteger[] publicKey = {n, e};
        BigInteger[] privateKey = {n, d};
        BigInteger[][] keys = {publicKey, privateKey};
        return keys;
    }
}
