
public class CaesarCipher {
    private String alphabet = "!#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[] ^_`abcdefghijklmnopqrstuvwxyz{|}~";

    public CaesarCipher(){

    }

    public CaesarCipher(String alphabet){
        this.alphabet = alphabet;
    }

    public String CaesarCipher(String message, Boolean encode, int key){
        String translated = "";
        int alphabetLength = alphabet.length();
        key = key % alphabetLength;
        if(message == null){
            System.out.println("Message was null");
            return "";
        }
        for(char i: message.toCharArray()){
            if(alphabet.indexOf(i) == -1){
                System.out.println("char not in alphabet: " + i);
            }
            if(alphabet.indexOf(i) != -1){
                int num = alphabet.indexOf(i);
                if(encode == true) {
                    num = num + key;
                }else{
                    num = num - key;
                }
                if(num < 0){
                    num = alphabet.length() + num;
                    num = Math.abs(num);
                }
                num = num % alphabetLength;
                translated = translated.concat(String.valueOf(alphabet.charAt(num)));
            }else{
                translated = translated.concat(String.valueOf(i));
            }
        }

        return translated;
    }
}
