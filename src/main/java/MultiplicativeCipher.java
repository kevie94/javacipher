import java.math.BigInteger;

public class MultiplicativeCipher {
    private String alphabet = "!#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[] ^_`abcdefghijklmnopqrstuvwxyz{|}~";

    public MultiplicativeCipher(){

    }

    public MultiplicativeCipher(String alphabet){
        this.alphabet = alphabet;
    }

    public String encode(String message, int key){
        boolean validKey = hasModularInverse(key, alphabet.length());
        if(validKey == false){
            return null;
        }
        String translated = "";
        int alphabetLength = alphabet.length();
        for(char i: message.toCharArray()){
            if(alphabet.indexOf(i) != -1){
                int num = alphabet.indexOf(String.valueOf(i));
                num = (num * key) % alphabetLength;
                translated = translated.concat(String.valueOf(alphabet.charAt(num)));
            }else{
                translated = translated.concat(String.valueOf(i));
            }
        }
        return translated;
    }

    public String decode(String message, int key){
        boolean validKey = this.hasModularInverse(key, alphabet.length());
        if(!validKey) {
            return null;
        } else {
            String translated = "";
            int alphabetLength = alphabet.length();

            // Big Integer class can calculate the modular inverse of a number.
            BigInteger bkey, mod, inv;
            bkey = new BigInteger((String.valueOf(key)));
            mod = new BigInteger((String.valueOf(alphabetLength)));
            inv = bkey.modInverse(mod);
            int invKey = inv.intValue();

            for (char i : message.toCharArray()) {
                if (alphabet.indexOf(i) != -1) {
                    int num = alphabet.indexOf(String.valueOf(i));
                    num = (num * invKey) % alphabetLength;
                    num = num % alphabetLength;
                    translated = translated.concat(String.valueOf(alphabet.charAt(num)));
                } else {
                    translated = translated.concat(String.valueOf(i));
                }
            }
            return translated;
        }
    }
    // this exists as the BigInteger modInverse function returns an exception if none exists, and handling that with the gui gave me a headache.
    public boolean hasModularInverse(int key, int length){
        BigInteger bKey = new BigInteger(Integer.toString(key));
        BigInteger mod = new BigInteger(( String.valueOf(length)));
        BigInteger gcd = bKey.gcd(mod);
        if(gcd.compareTo( new BigInteger("1")) == 0){
            return true;
        }else{
            return false;
        }
    }
}
