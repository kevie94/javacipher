import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Caoimhín on 24/01/2017.
 */
public class MultiplicativeCipherTest {
    MultiplicativeCipher multiplicativeCipher = new MultiplicativeCipher();
    @Test
    public void TestEncode() throws Exception {
        String message = "A computer would deserve to be called intelligent if it could deceive a human into believing that it was human.";
        int key = 7;
        String encoded = multiplicativeCipher.encode(message, key);
    }

    @Test
    public void TestDecode() throws Exception {
        String message = "A computer would deserve to be called intelligent if it could deceive a human into believing that it was human.";
        int key = 7;
        String encoded = multiplicativeCipher.encode(message, key);
        String decoded = multiplicativeCipher.decode(encoded, key);
    }

}