import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Caoimhín on 23/01/2017.
 */
public class TranspositionCipherTest {
    TranspositionCipher testTranspositionCipher;

    @Test
    public void TestEncrypt() throws Exception {
        testTranspositionCipher = new TranspositionCipher();
        String message = "Common sense is not so common.";
        int key = 8;
        String encoded = testTranspositionCipher.encrypt(message, key);
        assertEquals("Cenoonommstmme oo snnio. s s c", encoded);
    }


    @Test
    public void TestDecrypt() throws Exception {
        testTranspositionCipher = new TranspositionCipher();
        String message = "123456";
        int key = 5;
        String msg = testTranspositionCipher.encrypt(message, key);
        String decoded = testTranspositionCipher.decrypt(msg, key);

        assertEquals(message, decoded);
    }

}