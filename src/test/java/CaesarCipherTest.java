import static org.junit.Assert.*;
import org.junit.Test;

/**
 * Created by Caoimhín on 23/01/2017.
 */
public class CaesarCipherTest {

    public CaesarCipher CaesarTest;
    @Test
    public void TestCaesarCipher1() throws Exception {
        CaesarTest = new CaesarCipher();
        String message = "ABC";
        String eExpected = "BCD";
        assertEquals(eExpected, CaesarTest.CaesarCipher(message, true, 94));
    }

    @Test
    public void TestCaesarCipher2() throws Exception {
        CaesarTest = new CaesarCipher();
        String message = "ABC";
        String dExpected = "@AB";
        assertEquals(dExpected, CaesarTest.CaesarCipher(message, false, 94));
    }

    @Test
    public void TestCaesarCipher3() throws Exception {
        CaesarTest = new CaesarCipher();
        String message = "This Is A Test @ [][]";
        String encoded = CaesarTest.CaesarCipher(message, true, 94);
        String decoded = CaesarTest.CaesarCipher(encoded, false, 94);
        assertEquals(message, decoded);
    }

}