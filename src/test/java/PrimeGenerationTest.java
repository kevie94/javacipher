import org.junit.Test;

import java.math.BigInteger;

import static org.junit.Assert.*;

/**
 * Created by Caoimhín on 28/03/2017.
 */
public class PrimeGenerationTest {
    PrimeGeneration rMiller = new PrimeGeneration();
    @Test
    public void rabinMiller() throws Exception {
        BigInteger notPrime1 = new BigInteger("2342342342342323");
        boolean nPrime1 = rMiller.rabinMiller(notPrime1);
        BigInteger notPrime2 = new BigInteger("54645645645645");
        boolean nPrime2 = rMiller.rabinMiller(notPrime2);

        BigInteger isPrime1 = new BigInteger("2342342342342357");
        boolean yPrime1 = rMiller.rabinMiller(isPrime1);
        BigInteger isPrime2 = new BigInteger("42342342524353453");
        boolean yPrime2 = rMiller.rabinMiller(isPrime2);
        assertEquals(false, nPrime1);
        assertEquals(false, nPrime2);
        assertEquals(true, yPrime1);
        assertEquals(true, yPrime2);
    }

}