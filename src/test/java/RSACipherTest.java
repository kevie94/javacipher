import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Caoimhín on 13/03/2017.
 */
public class RSACipherTest {
    RSACipher cipher = new RSACipher();

    @Test
    public void encryptAndWriteToFile() throws Exception {
        cipher.encryptAndWriteToFile("test_message.txt", "test_encryption_output.txt", "test_publicKey.rsa");
    }

    @Test
    public void readFromFileAndDecrypt(){
        cipher.readFromFileAndDecrypt("test_encryption_output.txt", "test_decrypt_message.txt", "test_privateKey.rsa");
    }

}