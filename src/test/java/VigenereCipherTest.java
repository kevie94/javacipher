import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Caoimhín on 20/03/2017.
 */
public class VigenereCipherTest {
    VigenereCipher vigenereCipher;
    String message = "A computer would deserve to be called intelligent if it could deceive a human into believing that it was human.";

    @Test
    public void encryptDecrypt() throws Exception {
        vigenereCipher = new VigenereCipher();
        String encrypted = vigenereCipher.encrypt(message, "pizza");
        String encrypted2 = vigenereCipher.encrypt(message, "Caoimhin");
        assertNotEquals(message, encrypted);
        assertNotEquals(encrypted2, encrypted);
        String decrypted = vigenereCipher.decrypt(encrypted, "pizza");
        assertNotEquals(encrypted, decrypted);
        assertEquals(decrypted, message);
    }

}